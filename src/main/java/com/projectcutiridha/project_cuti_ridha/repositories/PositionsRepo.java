package com.projectcutiridha.project_cuti_ridha.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projectcutiridha.project_cuti_ridha.models.Positions;

@Repository
public interface PositionsRepo extends JpaRepository <Positions, Integer> {

}
