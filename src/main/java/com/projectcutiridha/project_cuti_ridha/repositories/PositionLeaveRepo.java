package com.projectcutiridha.project_cuti_ridha.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projectcutiridha.project_cuti_ridha.models.PositionLeave;


@Repository
public interface PositionLeaveRepo extends JpaRepository <PositionLeave, Integer> {

}
