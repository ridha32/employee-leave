package com.projectcutiridha.project_cuti_ridha.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projectcutiridha.project_cuti_ridha.models.BucketApproval;

@Repository
public interface BucketApprovalRepo extends JpaRepository <BucketApproval, Integer>{

}
