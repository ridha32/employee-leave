package com.projectcutiridha.project_cuti_ridha.controller;



	import java.util.ArrayList;
	import java.util.HashMap;
	import java.util.List;
	import java.util.Map;

	import javax.validation.Valid;

	import org.modelmapper.ModelMapper;
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.web.bind.annotation.DeleteMapping;
	import org.springframework.web.bind.annotation.GetMapping;
	import org.springframework.web.bind.annotation.PostMapping;
	import org.springframework.web.bind.annotation.PutMapping;
	import org.springframework.web.bind.annotation.RequestBody;
	import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.RequestParam;
	import org.springframework.web.bind.annotation.RestController;

	import com.projectcutiridha.project_cuti_ridha.dtos.UsersDto;
	import com.projectcutiridha.project_cuti_ridha.exception.ResourceNotFoundException;
	import com.projectcutiridha.project_cuti_ridha.models.Users;
	import com.projectcutiridha.project_cuti_ridha.repositories.UsersRepo;

	@RestController
	@RequestMapping("/api/users")
	public class UsersController {
		
		@Autowired
		UsersRepo usersRepo;
		
		ModelMapper modelMapper = new ModelMapper();
		
		@PostMapping("/create")
		public Map<String, Object> createUsersMapper(@Valid @RequestBody UsersDto body){ 
			
			Map<String, Object> result = new HashMap<String, Object>();
			
			Users usersEntity = new Users();
			usersEntity = modelMapper.map(body, Users.class);
			
			usersRepo.save(usersEntity);
			
			body.setIdUser(usersEntity.getIdUser());
			
			result.put("Message", "New Data Created.");
			result.put("Data", body);
			
			return result;
		}
		
		
		@GetMapping("/readall")
		public Map<String, Object> getAllUsersMapper(){ //dto
			
			Map<String, Object> result = new HashMap<String, Object>();
			List<Users> listAllUsers = usersRepo.findAll();
			
			List<UsersDto> listUsersDto = new ArrayList<UsersDto>();
			
			for(Users usersEntity : listAllUsers) {
				
				UsersDto usersDto = new UsersDto();
				usersDto = modelMapper.map(usersEntity, UsersDto.class);
				
				listUsersDto.add(usersDto);
			}
			
			result.put("Message", "Read All Success");
			result.put("Data", listUsersDto);
			result.put("Total Items", listAllUsers.size());
		
			return result;
		}
		
		@GetMapping("/read")
		public Map<String, Object> getUsers(@RequestParam(name = "usersId")Integer id){
			Map<String, Object> result = new HashMap<String, Object>();
			
			Users usersEntity = usersRepo.findById(id)
					.orElseThrow(() -> new ResourceNotFoundException("Users", "usersId", id));
			
			UsersDto usersDto = new UsersDto();
			usersDto = modelMapper.map(usersEntity, UsersDto.class);
			
			result.put("Message", "Finding DataSuccess");
			result.put("Data", usersDto);
			
			return result;
		}
		
		@PutMapping("/update")
		public Map<String, Object> updateUsersMapper(@Valid @RequestBody UsersDto body, @RequestParam(name = "usersId") Integer id){
			
			Map<String, Object> result = new HashMap<String, Object>();
			
			Users usersEntity = usersRepo.findById(id)
					.orElseThrow(() -> new ResourceNotFoundException("Users", "usersId", id));
			
			body.setIdUser(usersEntity.getIdUser());
			usersEntity = modelMapper.map(body, Users.class);
			
			usersRepo.save(usersEntity);
			
			result.put("Message", "Update success");
			result.put("data", body);
				
			return result;
		}
		
		@DeleteMapping("/delete")
	    public Map<String, Object> deleteUsersDto(@RequestParam(name = "usersId") Integer id){
	        Map<String, Object> result = new HashMap<String, Object>(); 
	        
	        Users usersEntity = usersRepo.findById(id)
	                 .orElseThrow(() -> new ResourceNotFoundException("Users", "usersId", id));
	        
	        UsersDto usersDto = new UsersDto();
	        usersDto = modelMapper.map(usersEntity, UsersDto.class);
	        
	        result.put("message", "Delete Data Success.");
	        result.put("Data", usersDto);
	        
	        usersRepo.deleteById(id);
	        
	        return result;
	    }

}
