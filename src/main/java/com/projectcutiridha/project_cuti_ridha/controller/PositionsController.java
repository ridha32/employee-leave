package com.projectcutiridha.project_cuti_ridha.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.projectcutiridha.project_cuti_ridha.dtos.PositionsDto;
import com.projectcutiridha.project_cuti_ridha.exception.ResourceNotFoundException;
import com.projectcutiridha.project_cuti_ridha.models.Positions;
import com.projectcutiridha.project_cuti_ridha.repositories.PositionsRepo;

	@RestController
	@RequestMapping("/api/positions")
	public class PositionsController {
		
		@Autowired
		PositionsRepo positionsRepo;
		
		ModelMapper modelMapper = new ModelMapper();
		
		@PostMapping("/create")
		public Map<String, Object> createPositionsMapper(@Valid @RequestBody PositionsDto body){ 
			
			Map<String, Object> result = new HashMap<String, Object>();
			
			Positions positionsEntity = new Positions();
			positionsEntity = modelMapper.map(body, Positions.class);
			
			positionsRepo.save(positionsEntity);
			
			body.setIdPosition(positionsEntity.getIdPosition());
			
			result.put("Message", "New Data Created.");
			result.put("Data", body);
			
			return result;
		}
		
		
		@GetMapping("/readall")
		public Map<String, Object> getAllPositionsMapper(){ //dto
			
			Map<String, Object> result = new HashMap<String, Object>();
			List<Positions> listAllPositions = positionsRepo.findAll();
			
			List<PositionsDto> listPositionsDto = new ArrayList<PositionsDto>();
			
			for(Positions positionsEntity : listAllPositions) {
				
				PositionsDto PositionsDto = new PositionsDto();
				PositionsDto = modelMapper.map(positionsEntity, PositionsDto.class);
				
				listPositionsDto.add(PositionsDto);
			}
			
			result.put("Message", "Read All Success");
			result.put("Data", listPositionsDto);
			result.put("Total Items", listAllPositions.size());
		
			return result;
		}
		
		@GetMapping("/read")
		public Map<String, Object> getPositions(@RequestParam(name = "positionsId")Integer id){
			Map<String, Object> result = new HashMap<String, Object>();
			
			Positions positionsEntity = positionsRepo.findById(id)
					.orElseThrow(() -> new ResourceNotFoundException("Positions", "positionsId", id));
			
			PositionsDto positionsDto = new PositionsDto();
			positionsDto = modelMapper.map(positionsEntity, PositionsDto.class);
			
			result.put("Message", "Finding DataSuccess");
			result.put("Data", positionsDto);
			
			return result;
		}
		
		@PutMapping("/update")
		public Map<String, Object> updatePositionsMapper(@Valid @RequestBody PositionsDto body, @RequestParam(name = "positionsId") Integer id){
			
			Map<String, Object> result = new HashMap<String, Object>();
			
			Positions positionsEntity = positionsRepo.findById(id)
					.orElseThrow(() -> new ResourceNotFoundException("Positions", "positionsId", id));
			
			body.setIdPosition(positionsEntity.getIdPosition());
			positionsEntity = modelMapper.map(body, Positions.class);
			
			positionsRepo.save(positionsEntity);
			
			result.put("Message", "Update category success");
			result.put("data", body);
				
			return result;
		}
		
		@DeleteMapping("/delete")
	    public Map<String, Object> deletePositionsDto(@RequestParam(name = "positionsId") Integer id){
	        Map<String, Object> result = new HashMap<String, Object>(); 
	        
	        Positions positionsEntity = positionsRepo.findById(id)
	                 .orElseThrow(() -> new ResourceNotFoundException("Positions", "positionsId", id));
	        
	        PositionsDto PositionsDto = new PositionsDto();
	        PositionsDto = modelMapper.map(positionsEntity, PositionsDto.class);
	        
	        result.put("message", "Delete Data Success.");
	        result.put("Data", PositionsDto);
	        
	        positionsRepo.deleteById(id);
	        
	        return result;
	    }
}
