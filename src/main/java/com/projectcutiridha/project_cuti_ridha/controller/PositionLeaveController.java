package com.projectcutiridha.project_cuti_ridha.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.projectcutiridha.project_cuti_ridha.dtos.PositionLeaveDto;
import com.projectcutiridha.project_cuti_ridha.exception.ResourceNotFoundException;
import com.projectcutiridha.project_cuti_ridha.models.PositionLeave;
import com.projectcutiridha.project_cuti_ridha.repositories.PositionLeaveRepo;
	
	@RestController
	@RequestMapping("/api/positionLeave")
	public class PositionLeaveController {
		
		@Autowired
		PositionLeaveRepo positionLeaveRepo;
		
		ModelMapper modelMapper = new ModelMapper();
		
		@PostMapping("/create")
		public Map<String, Object> createPositionLeaveMapper(@Valid @RequestBody PositionLeaveDto body){ 
			
			Map<String, Object> result = new HashMap<String, Object>();
			
			PositionLeave positionLeaveEntity = new PositionLeave();
			positionLeaveEntity = modelMapper.map(body, PositionLeave.class);
			
			positionLeaveRepo.save(positionLeaveEntity);
			
			body.setIdPositionLeave(positionLeaveEntity.getIdPositionLeave());
			
			result.put("Message", "New Data Created.");
			result.put("Data", body);
			
			return result;
		}
		
		
		@GetMapping("/readall")
		public Map<String, Object> getAllPositionLeaveMapper(){ //dto
			
			Map<String, Object> result = new HashMap<String, Object>();
			List<PositionLeave> listAllPositionLeave = positionLeaveRepo.findAll();
			
			List<PositionLeaveDto> listPositionLeaveDto = new ArrayList<PositionLeaveDto>();
			
			for(PositionLeave positionLeaveEntity : listAllPositionLeave) {
				
				PositionLeaveDto positionLeaveDto = new PositionLeaveDto();
				positionLeaveDto = modelMapper.map(positionLeaveEntity, PositionLeaveDto.class);
				
				listPositionLeaveDto.add(positionLeaveDto);
			}
			
			result.put("Message", "Read All Success");
			result.put("Data", listPositionLeaveDto);
			result.put("Total Items", listAllPositionLeave.size());
		
			return result;
		}
		
		@GetMapping("/read")
		public Map<String, Object> getPositionLeaveLeave(@RequestParam(name = "positionLeaveId")Integer id){
			Map<String, Object> result = new HashMap<String, Object>();
			
			PositionLeave positionLeaveEntity = positionLeaveRepo.findById(id)
					.orElseThrow(() -> new ResourceNotFoundException("PositionLeave", "positionLeaveId", id));
			
			PositionLeaveDto positionLeaveDto = new PositionLeaveDto();
			positionLeaveDto = modelMapper.map(positionLeaveEntity, PositionLeaveDto.class);
			
			result.put("Message", "Finding DataSuccess");
			result.put("Data", positionLeaveDto);
			
			return result;
		}
		
		@PutMapping("/update")
		public Map<String, Object> updatePositionLeaveMapper(@Valid @RequestBody PositionLeaveDto body, @RequestParam(name = "positionLeaveId") Integer id){
			
			Map<String, Object> result = new HashMap<String, Object>();
			
			PositionLeave positionLeaveEntity = positionLeaveRepo.findById(id)
					.orElseThrow(() -> new ResourceNotFoundException("PositionLeave", "positionLeaveId", id));
			
			body.setIdPositionLeave(positionLeaveEntity.getIdPositionLeave());
			positionLeaveEntity = modelMapper.map(body, PositionLeave.class);
			
			positionLeaveRepo.save(positionLeaveEntity);
			
			result.put("Message", "Update category success");
			result.put("data", body);
				
			return result;
		}
		
		@DeleteMapping("/delete")
	    public Map<String, Object> deletePositionLeaveDto(@RequestParam(name = "positionLeaveId") Integer id){
	        Map<String, Object> result = new HashMap<String, Object>(); 
	        
	        PositionLeave positionLeaveEntity = positionLeaveRepo.findById(id)
	                 .orElseThrow(() -> new ResourceNotFoundException("PositionLeave", "positionLeaveId", id));
	        
	        PositionLeaveDto positionLeaveDto = new PositionLeaveDto();
	        positionLeaveDto = modelMapper.map(positionLeaveEntity, PositionLeaveDto.class);
	        
	        result.put("message", "Delete Data Success.");
	        result.put("Data", positionLeaveDto);
	        
	        positionLeaveRepo.deleteById(id);
	        
	        return result;
	    }

}
