package com.projectcutiridha.project_cuti_ridha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectCutiRidhaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectCutiRidhaApplication.class, args);
	}

}
