package com.projectcutiridha.project_cuti_ridha.dtos;

import java.util.Date;

public class UsersDto {
	
	private int idUser;
	private PositionsDto positions;
	private String userName;
	private String userGender;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public UsersDto(int idUser, PositionsDto positions, String userName, String userGender, String createdBy,
			Date createdDate, String updatedBy, Date updatedDate) {
		super();
		this.idUser = idUser;
		this.positions = positions;
		this.userName = userName;
		this.userGender = userGender;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}
	
	public UsersDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public PositionsDto getPositions() {
		return positions;
	}
	public void setPositions(PositionsDto positions) {
		this.positions = positions;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserGender() {
		return userGender;
	}
	public void setUserGender(String userGender) {
		this.userGender = userGender;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
