package com.projectcutiridha.project_cuti_ridha.dtos;

import java.util.Date;

import com.projectcutiridha.project_cuti_ridha.models.Positions;

public class PositionLeaveDto {
	
	private int idPositionLeave;
	private Positions positions;
	private int totalLeave;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public PositionLeaveDto(int idPositionLeave, Positions positions, int totalLeave, String createdBy,
			Date createdDate, String updatedBy, Date updatedDate) {
		super();
		this.idPositionLeave = idPositionLeave;
		this.positions = positions;
		this.totalLeave = totalLeave;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	public PositionLeaveDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIdPositionLeave() {
		return idPositionLeave;
	}

	public void setIdPositionLeave(int idPositionLeave) {
		this.idPositionLeave = idPositionLeave;
	}

	public Positions getPositions() {
		return positions;
	}

	public void setPositions(Positions positions) {
		this.positions = positions;
	}

	public int getTotalLeave() {
		return totalLeave;
	}

	public void setTotalLeave(int totalLeave) {
		this.totalLeave = totalLeave;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
