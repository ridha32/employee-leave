package com.projectcutiridha.project_cuti_ridha.dtos;

import java.util.Date;

public class BucketApprovalDto {
	
	private int idBucketApproval;
	private UsersDto users;
	private String resolverReason;
	private String resolvedBy;
	private Date resolvedDate;
	private String status;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	public BucketApprovalDto(int idBucketApproval, UsersDto users, String resolverReason, String resolvedBy,
			Date resolvedDate, String status, String createdBy, Date createdDate, String updatedBy, Date updatedDate) {
		super();
		this.idBucketApproval = idBucketApproval;
		this.users = users;
		this.resolverReason = resolverReason;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
		this.status = status;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}
	public BucketApprovalDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getIdBucketApproval() {
		return idBucketApproval;
	}
	public void setIdBucketApproval(int idBucketApproval) {
		this.idBucketApproval = idBucketApproval;
	}
	public UsersDto getUsers() {
		return users;
	}
	public void setUsers(UsersDto users) {
		this.users = users;
	}
	public String getResolverReason() {
		return resolverReason;
	}
	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}
	public String getResolvedBy() {
		return resolvedBy;
	}
	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}
	public Date getResolvedDate() {
		return resolvedDate;
	}
	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	

}
