package com.projectcutiridha.project_cuti_ridha.dtos;

import java.util.Date;

public class UserLeaveRequestDto {
	
	private int idUserLeave;
	private BucketApprovalDto bucketApproval;
	private UsersDto users;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private String description;
	private Integer sisaCuti;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public UserLeaveRequestDto(int idUserLeave, BucketApprovalDto bucketApproval, UsersDto users, Date leaveDateFrom,
			Date leaveDateTo, String description, Integer sisaCuti, String createdBy, Date createdDate,
			String updatedBy, Date updatedDate) {
		super();
		this.idUserLeave = idUserLeave;
		this.bucketApproval = bucketApproval;
		this.users = users;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.description = description;
		this.sisaCuti = sisaCuti;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}
	
	public UserLeaveRequestDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIdUserLeave() {
		return idUserLeave;
	}

	public void setIdUserLeave(int idUserLeave) {
		this.idUserLeave = idUserLeave;
	}

	public BucketApprovalDto getBucketApproval() {
		return bucketApproval;
	}

	public void setBucketApproval(BucketApprovalDto bucketApproval) {
		this.bucketApproval = bucketApproval;
	}

	public UsersDto getUsers() {
		return users;
	}

	public void setUsers(UsersDto users) {
		this.users = users;
	}

	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	public Date getLeaveDateTo() {
		return leaveDateTo;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getSisaCuti() {
		return sisaCuti;
	}

	public void setSisaCuti(Integer sisaCuti) {
		this.sisaCuti = sisaCuti;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	
	
	
}
