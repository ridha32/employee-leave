package com.projectcutiridha.project_cuti_ridha.models;
// Generated Oct 20, 2020 10:28:08 PM by Hibernate Tools 4.3.5.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * BucketApproval generated by hbm2java
 */
@Entity
@Table(name = "bucket_approval", schema = "public")
public class BucketApproval implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2175130770513780372L;
	
	private int idBucketApproval;
	private Users users;
	private String resolverReason;
	private String resolvedBy;
	private Date resolvedDate;
	private String status;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	private Set<UserLeaveRequest> userLeaveRequests = new HashSet<UserLeaveRequest>(0);

	public BucketApproval() {
	}

	public BucketApproval(int idBucketApproval, String resolverReason, String resolvedBy, Date resolvedDate) {
		this.idBucketApproval = idBucketApproval;
		this.resolverReason = resolverReason;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
	}

	public BucketApproval(int idBucketApproval, Users users, String resolverReason, String resolvedBy,
			Date resolvedDate, String status, String createdBy, Date createdDate, String updatedBy, Date updatedDate,
			Set<UserLeaveRequest> userLeaveRequests) {
		this.idBucketApproval = idBucketApproval;
		this.users = users;
		this.resolverReason = resolverReason;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
		this.status = status;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
		this.userLeaveRequests = userLeaveRequests;
	}

	@Id

	@Column(name = "id_bucket_approval", unique = true, nullable = false)
	public int getIdBucketApproval() {
		return this.idBucketApproval;
	}

	public void setIdBucketApproval(int idBucketApproval) {
		this.idBucketApproval = idBucketApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_user")
	public Users getUsers() {
		return this.users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	@Column(name = "resolver_reason", nullable = false)
	public String getResolverReason() {
		return this.resolverReason;
	}

	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}

	@Column(name = "resolved_by", nullable = false, length = 30)
	public String getResolvedBy() {
		return this.resolvedBy;
	}

	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "resolved_date", nullable = false, length = 13)
	public Date getResolvedDate() {
		return this.resolvedDate;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	@Column(name = "status", length = 15)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", length = 29)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_by")
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date", length = 29)
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bucketApproval")
	public Set<UserLeaveRequest> getUserLeaveRequests() {
		return this.userLeaveRequests;
	}

	public void setUserLeaveRequests(Set<UserLeaveRequest> userLeaveRequests) {
		this.userLeaveRequests = userLeaveRequests;
	}

}
