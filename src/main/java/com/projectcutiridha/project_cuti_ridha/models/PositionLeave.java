package com.projectcutiridha.project_cuti_ridha.models;
// Generated Oct 20, 2020 10:28:08 PM by Hibernate Tools 4.3.5.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * PositionLeave generated by hbm2java
 */
@Entity
@Table(name = "position_leave", schema = "public")
public class PositionLeave implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 733220145463593834L;
	
	private int idPositionLeave;
	private Positions positions;
	private int totalLeave;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;

	public PositionLeave() {
	}

	public PositionLeave(int idPositionLeave, int totalLeave) {
		this.idPositionLeave = idPositionLeave;
		this.totalLeave = totalLeave;
	}

	public PositionLeave(int idPositionLeave, Positions positions, int totalLeave, String createdBy, Date createdDate,
			String updatedBy, Date updatedDate) {
		this.idPositionLeave = idPositionLeave;
		this.positions = positions;
		this.totalLeave = totalLeave;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_positionsleave_id_positionsleave_seq")
	@SequenceGenerator(name = "generator_positionsleave_id_positionsleave_seq", sequenceName = "position_leave_seq", schema = "public", allocationSize = 1 )
	@Column(name = "id_position_leave", unique = true, nullable = false)
	public int getIdPositionLeave() {
		return this.idPositionLeave;
	}

	public void setIdPositionLeave(int idPositionLeave) {
		this.idPositionLeave = idPositionLeave;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_position")
	public Positions getPositions() {
		return this.positions;
	}

	public void setPositions(Positions positions) {
		this.positions = positions;
	}

	@Column(name = "total_leave", nullable = false)
	public int getTotalLeave() {
		return this.totalLeave;
	}

	public void setTotalLeave(int totalLeave) {
		this.totalLeave = totalLeave;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", length = 29)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_by")
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date", length = 29)
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
